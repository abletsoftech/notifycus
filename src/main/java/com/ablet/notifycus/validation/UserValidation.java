package com.ablet.notifycus.validation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.ablet.notifycus.model.CreateOrder;
import com.ablet.notifycus.model.CreditPlan;
import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.Otp;
import com.ablet.notifycus.model.SchedulerHistory;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.repositories.CreditPlanRepository;
import com.ablet.notifycus.repositories.CustomerRepository;
import com.ablet.notifycus.repositories.UserRepository;
import com.ablet.notifycus.service.UserService;
import com.ablet.notifycus.utils.ExceptionHandling;

@Component
public class UserValidation {
	Logger logs = LoggerFactory.getLogger(UserValidation.class);
	@Autowired
	private InputValidation inputValidation;
	@Autowired
	private CreateOrder createOrder;
	@Autowired
	private UserService userService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private CreditPlanRepository creditPlanRepository;
	@Autowired
	private ExceptionHandling exceptionHandling;
	@Autowired
	private PasswordEncoder passwordEncoder;

	private String response;

	public String userLogin(String email, String password) {
		try {
			response = inputValidation.login(email, password);
			if (response.equalsIgnoreCase("ok")) {
				return passwordEncoder.matches(password, userRepository.userLogin(email)) ? "ok"
						: "Invalid Credentials";
			} else {
				return response;
			}
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;

		}

	}
	public String adminLogin(String email, String password) {
		try {
			response = inputValidation.login(email, password);
			if (response.equalsIgnoreCase("ok")) {
				return passwordEncoder.matches(password, userRepository.adminLogin(email)) ? "ok"
						: "Invalid Credentials";
			} else {
				return response;
			}
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;

		}

	}

	public User registrationOtp(User user) {
		try {
			response = inputValidation.registrationOtp(user);
			if (response.equalsIgnoreCase("ok")) {
				user.setMessage(userService.registrationOtp(user));
				return user;
			} else {
				user.setMessage(response);
				return user;
			}
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			user.setMessage(exceptionHandling.sysExMsg);
			return user;

		}

	}

	public String userRegistration(User user) {
		try {
			response = inputValidation.userRegistration(user);
			return response.equalsIgnoreCase("ok") ? userService.userRegistration(user) : response;
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;
		}

	}

	public String resetPasswordOtp(String email) {
		try {
			response = inputValidation.resetPasswordOtp(email);
			return response.equalsIgnoreCase("ok") ? userService.resetPasswordOtp(email) : response;
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;
		}
	}

	public String resetPassword(User user) {
		try {
			response = inputValidation.resetPassword(user);
			return response.equalsIgnoreCase("ok") ? userService.resetPassword(user) : response;
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;
		}

	}

	public String addCustomerDetails(Customer customer, HttpServletRequest request) {
		try {
			customer.setUser(userRepository.findByEmail(request.getHeader("email")));
			response = inputValidation.addCustomerDetails(customer);
			return response.equalsIgnoreCase("ok") ? userService.addCustomerDetails(customer) : response;
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;
		}

	}

	public String updateCustomerDetails(Customer customer, HttpServletRequest request) {
		try {
			customer.setUser(userRepository.findByEmail(request.getHeader("email")));
			response = inputValidation.updateCustomerDetails(customer, request);
			return response.equalsIgnoreCase("ok") ? userService.updateCustomerDetails(customer) : response;
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;
		}

	}

	public String deleteCustomerDetails(Customer customer, HttpServletRequest request) {
		try {
			customer.setUser(userRepository.findByEmail(request.getHeader("email")));
			response = inputValidation.deleteCustomerDetails(customer, request);
			return response.equalsIgnoreCase("ok") ? userService.updateCustomerDetails(customer) : response;
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return exceptionHandling.sysExMsg;
		}

	}

	public int totalTodaysCustForUser(HttpServletRequest request) {
		try {
			return customerRepository
					.customerDetailsByToday(userRepository.findByEmail(request.getHeader("email")).getUserID()).size();
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return 0;
		}
	}

	public int numOfTotalCusForUser(HttpServletRequest request) {
		try {
			return customerRepository
					.customerDetailsForUser(userRepository.findByEmail(request.getHeader("email")).getUserID()).size();
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return 0;
		}
	}

	public List<Customer> customerDetailsForUser(HttpServletRequest request) {
		try {
			return userService
					.customerDetailsForUser(userRepository.findByEmail(request.getHeader("email")).getUserID());
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return null;
		}

	}

	public List<Customer> pucExpTodayForUser(HttpServletRequest request) {
		try {
			return customerRepository
					.pucExpireTodayByUserID(userRepository.findByEmail(request.getHeader("email")).getUserID());
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return null;
		}
	}

	public CreateOrder buyCredit(String planName, HttpServletRequest request) {
		try {

			CreditPlan creditPlan = creditPlanRepository.findByPlanName(planName);
			createOrder.setUser(userRepository.findByEmail(request.getHeader("email")));
			createOrder.setTotalAmount(creditPlan.getAmount());
			createOrder.setTotalCredit(creditPlan.getTotalCredit());
			return userService.buyCredit(createOrder);
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return null;
		}

	}

	public String paymentVerification(CreateOrder createOrder, HttpServletRequest request) {
		try {
			createOrder.setUser(userRepository.findByEmail(request.getHeader("email")));
			response = inputValidation.paymentVerification(createOrder);
			return response.equalsIgnoreCase("ok") ? userService.paymentVerification(createOrder) : response;
		} catch (Exception ex) {
			exceptionHandling.exceptionTrace(ex);
			return ex.getMessage();
		}

	}
}
