package com.ablet.notifycus.validation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.SchedulerHistory;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.repositories.CreditPlanRepository;
import com.ablet.notifycus.repositories.CustomerRepository;
import com.ablet.notifycus.repositories.UserRepository;
import com.ablet.notifycus.service.UserService;

@Component
public class AdminValidation {
	
	Logger logs = LoggerFactory.getLogger(AdminValidation.class);
	@Autowired
	private InputValidation inputValidation;
	@Autowired
	private UserService userService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private CreditPlanRepository creditPlanRepository;

	
	public List<SchedulerHistory> getSechdulerHistoryFromDatabase() {
		try {
			return userService.getSechdulerHistoryFromDatabase();
		} catch (Exception e) {
			logs.error(e.getMessage());
			return null;
		}
	}
	public List<Customer> getAllCustomersDetailsWhosePlanExpiryWithin3Days() {
		try {
			return userService.planExpiryWithin3Days();
		} catch (Exception ex) {
			logs.error(ex.toString());
			ex.printStackTrace();
			return null;
		}

	}

	
	public List<User> getAllUserDetails() {
		try {
			return userService.getAllUserDetails();
		} catch (Exception e) {
			logs.error(e.getMessage());
			return null;
		}

	}

}
