package com.ablet.notifycus.validation;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class PatternValidation {

	public boolean emailValidation(String email) {
		String pattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		return Pattern.matches(pattern, email);

	}

	public boolean nameValidation(String name) {
		String pattern = "[a-zA-Z_0-9\\s]{2,25}";
		return Pattern.matches(pattern, name);

	}

	public boolean validateCompanyName(String cname) {
		String pattern = "[a-zA-Z_0-9\\s\\D]{1,50}";// max length 50
		return Pattern.matches(pattern, cname);

	}

	public boolean passwordValidation(String password) {
		//String pattern = "((?=.*\\d)(?=.*\\w)(?=.*[@#$%&]).{8,15})";//(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}
		String pattern = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).{8,15}";
		return Pattern.matches(pattern, password);

	}

	public boolean mobile_NoValidation(String mobileNo) {
		String pattern = "[6789]{1}[0-9]{9}";
		return Pattern.matches(pattern, mobileNo);

	}

	public boolean otpValidation(String otp) {
		String pattern = "[0-9]{6}";
		return Pattern.matches(pattern, otp);

	}


}
