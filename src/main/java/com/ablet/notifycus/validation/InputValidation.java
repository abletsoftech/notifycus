package com.ablet.notifycus.validation;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ablet.notifycus.model.CreateOrder;
import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.repositories.CreateOrderRepository;
import com.ablet.notifycus.repositories.CustomerRepository;
import com.ablet.notifycus.repositories.OtpRepository;
import com.ablet.notifycus.repositories.UserCreditRepository;
import com.ablet.notifycus.repositories.UserRepository;
import com.ablet.notifycus.service.UserService;

@Component
public class InputValidation {
	@Autowired
	private PatternValidation patternValidation;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private OtpRepository otpRepository;
	@Autowired
	private CreateOrderRepository createOrderRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private UserCreditRepository userCreditRepository;
	Logger logs = LoggerFactory.getLogger(InputValidation.class);
	
	public String login(String email, String password) {
		if (!patternValidation.emailValidation(email))
			return "Invalid Credentials";
		if (!patternValidation.passwordValidation(password))
			return "Invalid Credentials";
		return "ok";
	}

	public String registrationOtp(User user) {
		if (userRepository.existsByEmail(user.getEmail()))
			return "You are already Registerd user";
		if (user.getEmail() == null || !(patternValidation.emailValidation(user.getEmail())))
			return "enter valid email";
		if (user.getCompanyName().length() > 25)
			return "Company_Name must not be exceed 25 character";
		if (user.getName() == null || !(patternValidation.nameValidation(user.getName())))
			return "enter valid Name";
		if (user.getMobileNo() == null || !(patternValidation.mobile_NoValidation(user.getMobileNo())))
			return "enter valid mobile_no";
		if (user.getPassword() == null || !(patternValidation.passwordValidation(user.getPassword())))
			return "enter valid password";
		return "ok";

	}

	public String userRegistration(User user) {
		if (userRepository.existsByEmail(user.getEmail()))
			return "You are already Registerd user";
		if (user.getCompanyName().length() > 25)
			return "Company_Name must not exceed 25 character";
		if (user.getEmail() == null || !(patternValidation.emailValidation(user.getEmail())))
			return "enter valid email";
		if (user.getName() == null || !(patternValidation.nameValidation(user.getName())))
			return "enter valid Name";
		if (user.getMobileNo() == null || !(patternValidation.mobile_NoValidation(user.getMobileNo())))
			return "enter valid mobile_no";
		if (user.getPassword() == null || !(patternValidation.passwordValidation(user.getPassword())))
			return "enter valid password";
		if (user.getOtp() == null && !(patternValidation.otpValidation(user.getOtp())))
			return "invalid otp";
		if (otpRepository.findByEmailAndOtp(user.getEmail(), user.getOtp()) == null)
			return "otp time_expire";
		return "ok";
	}

	public String getUserDetailsByEmail(String email) {
		if (!patternValidation.emailValidation(email))
			return "enter valid email";
		return "ok";
	}

	public String resetPasswordOtp(String email) {
		if (!patternValidation.emailValidation(email))
			return "Please enter valid email";
		if (!(userRepository.existsByEmail(email)))
			return "You are not registerd user, Please registeration first";
		return "ok";
	}

	public String resetPassword(User user) {
		if (!patternValidation.emailValidation(user.getEmail()))
			return "enter valid email";
		if (!(userRepository.existsByEmail(user.getEmail())))
			return "You are not Registerd_user,Please Registerd first";
		if (!patternValidation.passwordValidation(user.getPassword()))
			return "enter valid password";
		if (otpRepository.findByEmailAndOtp(user.getEmail(), user.getOtp()) == null)
			return "otp time_expire";
		return "ok";
	}

	public String addCustomerDetails(Customer customer) {

		if (userCreditRepository.totalUserCredit(customer.getUser().getUserID())>0)
			return "Low Credit, Please buy Credit";
		if (customer.getMobileNo() == null || !(patternValidation.mobile_NoValidation(customer.getMobileNo())))
			return "enter valid mobile_no";
		if (customer.getExpiryDurationMonth() <= 0)
			return "enter expiry month";
		if (customer.getVehicleNo() == null|| customer.getVehicleNo().isBlank())
			return "enter vehicle_number";
		if (customer.getVehicleType() == null || customer.getVehicleNo().isBlank())
			return "enter vehicle_type";
		if (customer.getWorkType() == null || customer.getVehicleNo().isBlank())
			return "enter work_type(like Pollution or Insurance etc)";
		if (customer.getAmount() <=0)
			return "enter valid amount";
		if (customer.getExpiryDate()==null || !(customer.getExpiryDate().toString().matches("\\d{4}-\\d{2}-\\d{2}")))
			return "enter valid date";
		return "ok";
	}
	public String updateCustomerDetails(Customer customer,HttpServletRequest request) {
		if((userRepository.findByEmail(request.getHeader("email")).getUserID()!=customerRepository.findByCustomerID(customer.getCustomerID()).getUser().getUserID()))
			return "Malicious_Request";
		if (customer.getMobileNo() == null || !(patternValidation.mobile_NoValidation(customer.getMobileNo())))
			return "enter valid mobile_no";
		if (customer.getExpiryDurationMonth() <= 0)
			return "enter expiry month";
		if (customer.getVehicleNo() == null|| customer.getVehicleNo().isBlank())
			return "enter vehicle_number";
		if (customer.getVehicleType() == null || customer.getVehicleNo().isBlank())
			return "enter vehicle_type";
		if (customer.getWorkType() == null || customer.getVehicleNo().isBlank())
			return "enter work_type(like Pollution or Insurance etc)";
		if (customer.getAmount() <=0)
			return "enter valid amount";
		if (customer.getExpiryDate()==null || !(customer.getExpiryDate().toString().matches("\\d{4}-\\d{2}-\\d{2}")))
			return "enter valid date";
		return "ok";
	}
	public String deleteCustomerDetails(Customer customer,HttpServletRequest request) {
		if((userRepository.findByEmail(request.getHeader("email")).getUserID()!=customerRepository.findByCustomerID(customer.getCustomerID()).getUser().getUserID()))
			return "Malicious_Request";
		return "ok";
	}

	public String paymentVerification(CreateOrder createOrder) {
		if (createOrderRepository.findBycreateOrderIDAndUserID(createOrder.getCreateOrderID(),
				createOrder.getUser().getUserID()) == null)
			return "Invalid Order_ID";
		return "ok";

	}

}
