package com.ablet.notifycus.mail;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.sun.istack.NotNull;

@Component
@PropertySource("classpath:application.properties")
public class SendMail {
	@Value("${spring.variable.emailID}")
	@NotNull // optional
	private String emailID;
	@Value("${spring.variable.password}")
	@NotNull // optional
	private String password;
	@Value("${spring.variable.companyName}")
	@NotNull // optional
	private String companyName;


	/**
	 * Outgoing Mail (SMTP) Server requires TLS or SSL: smtp.gmail.com (use
	 * authentication) Use Authentication: Yes Port for SSL: 465
	 * 
	 * @return
	 */
	private Session session() {
		Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "587");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true"); // TLS
		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				// return new PasswordAuthentication(emailID, password);
				return new PasswordAuthentication("princepr4294@gmail.com", "Prince123@");
			}
		});
		return session;
	}

	public void sentOtp(String email, String body, boolean response) throws Exception {
		MimeMessage msg = new MimeMessage(session());
		msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
		msg.addHeader("Content-Transfer-Encoding", "8bit");
		msg.setFrom(new InternetAddress(emailID, companyName));
		msg.setReplyTo(InternetAddress.parse(emailID, false));
		if (response)
			msg.setSubject("Verify Your Notifycus Account", "UTF-8");
		else
			msg.setSubject("Reset Your Notifycus Account Password", "UTF-8");
		msg.setContent(body.toString(), "text/html");
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
		Transport.send(msg);
	}

}
