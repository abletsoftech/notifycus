package com.ablet.notifycus.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.ablet.notifycus.model.User;
import com.ablet.notifycus.repositories.UserRepository;
import com.sun.istack.NotNull;

@Component
@PropertySource("classpath:application.properties")
public class FileBodyOperation {
	Logger logs = LoggerFactory.getLogger(FileBodyOperation.class);

	public String getFileBody(boolean response) throws IOException {
		String content=null;
		File file = null;
		if (response) {
			file = ResourceUtils.getFile("classpath:templates/register.txt");
			content = new String(Files.readAllBytes(file.toPath()));
		} else {
			file = ResourceUtils.getFile("classpath:templates/reset.txt");
			content = new String(Files.readAllBytes(file.toPath()));
		}

		return content;
	}


	public String setOtpEmail(String email, String otp, boolean response) throws IOException {
		String registerText = getFileBody(response);
		String temp1 = registerText.replace("name", email);
		String temp2 = temp1.replace("otp", otp);
		return temp2;

	}

}
