package com.ablet.notifycus.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mashape.unirest.http.Unirest;

public class TaskExecutor implements Runnable {
	Logger logs = LoggerFactory.getLogger(TaskExecutor.class);
	private String url;

	public TaskExecutor(String url) {
		this.url = url;
	}

	public void run() {
		try {
			@SuppressWarnings("unused")
			com.mashape.unirest.http.HttpResponse<String> response = Unirest.get(this.url)
					.header("cache-control", "no-cache").asString();
		} catch (Exception e) {
			logs.error(e.getMessage());
			e.printStackTrace();
		}

	}
}
