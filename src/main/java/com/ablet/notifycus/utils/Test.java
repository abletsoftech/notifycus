package com.ablet.notifycus.utils;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

public class Test {

	public static void isPrime(int num) {
		boolean isPrime=true;
		for(int i=2;i<=num/2;i++) {
			if(num%i==0)
				isPrime=false;
		}
		
		if(isPrime)
		   System.out.println(num + " is a Prime Number");
		else
		   System.out.println(num + " is not a Prime Number");
	}
	
	public static void main(String[] args) throws Exception {
		
		int[] arr= {1,2,3,5,2};
	//	List<Integer> list = Arrays.stream(arr).boxed().collect(Collectors.toList());
		int i=Arrays.stream(arr).max().getAsInt();
		List<Integer> list=Arrays.stream(arr).boxed().collect(Collectors.toList());
		Arrays.parallelSort(arr);
		String str[] = { "Homer", "Marge", "Bart", "Lisa", "Maggie" };
		List<String> lst = Arrays.asList(str);
		lst.sort(null);
		List<String> ls=new ArrayList<String>(Arrays.asList(str));
		// ls.forEach(games -> System.out.println(games)); 
		 ls.forEach(g ->System.out.println(g));
		 ls.sort((String s,String s1)->s.compareTo(s1));
		 DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		 LocalDateTime dt = LocalDateTime.parse("Fri, 22 Oct 2021, 04:59 PM", fmt);
	        // the date/time is in the default timezone
	        //dt.atZone(ZoneId.of("Asia/Kolkata"));
	        System.out.println(Timestamp.valueOf(dt));
	}
	
}
