package com.ablet.notifycus.utils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class LocalDateDeserializer  extends JsonDeserializer<LocalDate>{
	 private DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	@Override
	public LocalDate deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		fmt.withZone(ZoneId.of("Asia/Kolkata"));
		return LocalDate.parse(jsonParser.getText(), fmt);
	        
	}

}
