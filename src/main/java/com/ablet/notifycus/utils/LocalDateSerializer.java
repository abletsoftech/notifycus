package com.ablet.notifycus.utils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class LocalDateSerializer extends JsonSerializer<LocalDate>{
	 private DateTimeFormatter fmt = DateTimeFormatter.ofPattern("E, dd MMM yyyy");
	@Override
	public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		fmt.withZone(ZoneId.of("Asia/Kolkata"));
	    gen.writeString(fmt.format(value));
	}

}
