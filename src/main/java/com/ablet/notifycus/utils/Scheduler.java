package com.ablet.notifycus.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;

import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.SchedulerHistory;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.repositories.CustomerRepository;
import com.ablet.notifycus.repositories.SchedulerHistoryRepository;
import com.ablet.notifycus.repositories.UserRepository;
import com.sun.istack.NotNull;

@PropertySource("classpath:application.properties")
public class Scheduler {
	Logger logs = LoggerFactory.getLogger(Scheduler.class);

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private SchedulerHistoryRepository schedulerHistoryRepository;
	@Value("${spring.variable.bulkSmsUrl}")
	@NotNull // optional
	private String bulkSmsUrl;
	@Value("${spring.variable.messageTemp1}")
	@NotNull // optional
	private String messageTemp1;
	@Value("${spring.variable.messageTemp2}")
	@NotNull // optional
	private String messageTemp2;

	@Scheduled(cron = "0 0 11 * * *")
	public void sendPolicyExpireNotificationForNext1Days() {
		try {
			List<Customer> customer = customerRepository.pucExpiryWithinNext1Days();
			if (customer.size() > 0) {
				int sizeOfPool;
				if (customer.size() < 10)
					sizeOfPool = 1;
				else
					sizeOfPool = customer.size() / 5;

				ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(sizeOfPool);
				for (int i = 0; i < customer.size(); i++) {
					User user = userRepository.findByUserID(customer.get(i).getUser().getUserID());
					String temp4 = messageTemp1.replace("vehicleNo", customer.get(i).getVehicleNo());
					String temp5 = temp4.replace("subject", customer.get(i).getWorkType());
					String temp6 = temp5.replace("companyName", user.getCompanyName());
					String temp7 = temp6.replace("mobileNo", user.getMobileNo());
					String temp8 = bulkSmsUrl.replace("body", temp7);
					String temp9 = temp8.replace("sentTO", customer.get(i).getMobileNo());
					Runnable task = new TaskExecutor(temp9);
					executor.execute(task);
					Customer cust = customer.get(i);
					cust.setNotify2(true);
					customerRepository.saveAndFlush(cust);
				}
				executor.shutdown();
				SchedulerHistory schedulerHistory = new SchedulerHistory();
				schedulerHistory.setNext1DayScheduler(new Timestamp(System.currentTimeMillis()));
				schedulerHistoryRepository.save(schedulerHistory);

			}
		} catch (Exception e) {
			logs.error(e.getMessage());
			e.printStackTrace();

		}

	}

	@Scheduled(cron = "0 15 11 * * *")
	public void sendPolicyExpireNotificationForToday() {
		try {
			List<Customer> customer = customerRepository.pucExpireToday();
			if (customer.size() > 0) {
				int sizeOfPool;
				if (customer.size() < 10)
					sizeOfPool = 1;
				else 
					sizeOfPool = customer.size() / 5;
				
				ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(sizeOfPool);
				for (int i = 0; i < customer.size(); i++) {
					User user = userRepository.findByUserID(customer.get(i).getUser().getUserID());
					String temp4 = messageTemp2.replace("vehicleNo", customer.get(i).getVehicleNo());
					String temp5 = temp4.replace("subject", customer.get(i).getWorkType());
					String temp3 = user.getCompanyName().replace(" ", "%20");
					String temp6 = temp5.replace("companyName", temp3);
					String temp7 = temp6.replace("mobileNo", user.getMobileNo());
					String temp8 = bulkSmsUrl.replace("body", temp7);
					String temp9 = temp8.replace("sentTO", customer.get(i).getMobileNo());
					Runnable task = new TaskExecutor(temp9);
					executor.execute(task);
					Customer cust = customer.get(i);
					cust.setNotify2(true);
					customerRepository.saveAndFlush(cust);
				}
				executor.shutdown();
				if (schedulerHistoryRepository.getechdulerHistoryOnToday() != null) {
					SchedulerHistory schedulerHistory = schedulerHistoryRepository.getechdulerHistoryOnToday();
					schedulerHistory.setTodayScheduler(new Timestamp(System.currentTimeMillis()));
					schedulerHistoryRepository.saveAndFlush(schedulerHistory);
				} else {
					SchedulerHistory schedulerHistory = new SchedulerHistory();
					schedulerHistory.setTodayScheduler(new Timestamp(System.currentTimeMillis()));
					schedulerHistoryRepository.saveAndFlush(schedulerHistory);
				}
			}
		} catch (Exception e) {
			logs.error(e.getMessage());
			e.printStackTrace();
		}

	}

	// @Scheduled(cron = "0 39 18 * * *")
	// run and check validUPTO and credit will be 0
	// @Scheduled(cron = "*/5 * * * *", zone = "Asia/Kolkata")
	public void tes() {
		logs.error("cron job");

	}

}
