package com.ablet.notifycus.utils;

import java.util.Random;

import org.springframework.stereotype.Component;
@Component
public class OtpGenerator {
	
	public static String generateOtp() {
		String otp="";
		Random random=new Random();
		for(int i=0;i<6;i++) {
			otp=otp+random.nextInt(9);
		}
		return otp;
	}

}
