package com.ablet.notifycus.utils;

import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

@Component
public class Timestamps {

	public static Timestamp getTimestamp(String time) throws Exception {
		SimpleDateFormat datetimeFormatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date lFromDate1 = datetimeFormatter1.parse(time);
		Timestamp fromTS1 = new Timestamp(lFromDate1.getTime());
		return fromTS1;
	}

	public static LocalDate addFreeCreditMonth() throws Exception {
		ZoneId zoneId = ZoneId.of("Asia/Kolkata");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, 1);
		return LocalDate.ofInstant(calendar.toInstant(), zoneId);
	}

	public static LocalDate addMonthInCurrentDate(int month) throws Exception {
		ZoneId zoneId = ZoneId.of("Asia/Kolkata");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		formatter.withZone(zoneId);
		LocalDate date = LocalDate.parse(LocalDate.now().toString(),formatter);
		date = date.plusMonths(month);
		return date;
	}

	public static LocalDate addMonthInLocalDate(String localDate,int month) throws Exception {
		ZoneId zoneId = ZoneId.of("Asia/Kolkata");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		formatter.withZone(zoneId);
		LocalDate date = LocalDate.parse(localDate,formatter);
		date = date.plusMonths(month);
		return date;
	}
	
	public static void main(String[]a) throws Exception {
		System.out.println(addMonthInLocalDate("2021-02-04",6));
	}


}
