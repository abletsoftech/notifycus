package com.ablet.notifycus.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ExceptionHandling {
	Logger logs = LoggerFactory.getLogger(ExceptionHandling.class);

	public String sysExMsg = "System_Error: retry after sometime";

	public void exceptionTrace(Exception ex) {
		logs.error(ex.toString());
		ex.printStackTrace();

	}

}
