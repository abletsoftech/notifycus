package com.ablet.notifycus.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import com.ablet.notifycus.model.CreateOrder;
import com.ablet.notifycus.repositories.CreateOrderRepository;
import com.ablet.notifycus.service.UserService;
import com.razorpay.Order;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.sun.istack.NotNull;

@Component
@ConfigurationProperties
@PropertySource("classpath:application.properties")
public class CreateNewOrder {
	static Logger logs = LoggerFactory.getLogger(CreateNewOrder.class);
	@Autowired
	private CreateOrderRepository createOrderRepository;
	private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
	@Value("${spring.variable.keyID}")
	@NotNull // optional
	private String key_id;
	@Value("${spring.variable.keySecret}")
	@NotNull // optional
	private String key_secret;

	public CreateOrder createOrder(CreateOrder createOrder)  {
		try {
		RazorpayClient razorpay = new RazorpayClient(key_id, key_secret);
		JSONObject orderRequest = new JSONObject();
		orderRequest.put("amount", createOrder.getTotalAmount() * 100); // amount in the smallest currency unit
		orderRequest.put("currency", "INR");
		orderRequest.put("receipt", "order_" + createOrder.getUser().getUserID());
		Order order = razorpay.Orders.create(orderRequest);
		createOrder.setCreateOrderID(order.get("id"));
		return createOrder;
		}catch(Exception ex) {
			logs.error(ex.getMessage());
			ex.printStackTrace();
		}
		return null;
	}

	public static String calculateRFC2104HMAC(String data, String secret)  {

		String result = null;
		try {
			// get an hmac_sha256 key from the raw secret bytes
			SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(), HMAC_SHA256_ALGORITHM);

			// get an hmac_sha256 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
			mac.init(signingKey);

			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(data.getBytes());

			// base64-encode the hmac
			result = DatatypeConverter.printHexBinary(rawHmac).toLowerCase();
			return result;
		} catch (Exception ex) {
			logs.error(ex.getMessage());
			ex.printStackTrace();
		}
		return result;
	}

	public boolean paymentVerification(CreateOrder createOrder)  {

		try {
			String generated_signature = calculateRFC2104HMAC(
					createOrder.getCreateOrderID() + "|" + createOrder.getPaymentID(), key_secret);
			if (generated_signature == createOrder.getSignature())
				return capturePayments(createOrder);
		} catch (Exception ex) {
			logs.error(ex.getMessage());
			ex.printStackTrace();
		}
		return false;

	}

	public boolean capturePayments(CreateOrder createOrder) {
		try {
			RazorpayClient razorpay = new RazorpayClient(key_id, key_secret);
			createOrder = createOrderRepository.findBycreateOrderID(createOrder.getCreateOrderID());
			JSONObject captureRequest = new JSONObject();
			captureRequest.put("amount", createOrder.getTotalAmount() * 100);
			captureRequest.put("currency", "INR");
			razorpay.Payments.capture("<payment_id>", captureRequest);
			return true;
		} catch (RazorpayException ex) {
			logs.error(ex.getMessage());
			ex.printStackTrace();
		}

		return false;

	}

}
