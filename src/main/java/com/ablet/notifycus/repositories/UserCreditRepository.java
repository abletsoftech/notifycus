package com.ablet.notifycus.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.UserCredit;

public interface UserCreditRepository extends JpaRepository<UserCredit, Integer>{
	
	
	@Query(value = "SELECT SUM(REMAINING_CREDIT) from usercredit  where USER_ID=?1  AND EXP_DATE>=curdate() AND ACTIVATED=true", nativeQuery = true)
	int totalUserCredit(int userID);
	
	@Query(value = "SELECT * FROM usercredit  WHERE USER_ID=?1 AND REMAINING_CREDIT<CREDIT AND USED_CREDIT>0 AND EXP_DATE>=curdate() AND ACTIVATED=true", nativeQuery = true)
     UserCredit currentActiveUserPlan(int userID);
	
	@Query(value = "SELECT * FROM usercredit  WHERE USER_ID=?1 AND REMAINING_CREDIT<=CREDIT AND USED_CREDIT<=0 AND EXP_DATE>=curdate() AND ACTIVATED=true", nativeQuery = true)
	List<UserCredit> allActiveUserPlan(int userID);
	
	@Query(value = "SELECT * from usercredit  where USER_ID=?1", nativeQuery = true)
	List<UserCredit> userCreditHistory(int userID);

}
