package com.ablet.notifycus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ablet.notifycus.model.Otp;

public interface OtpRepository extends JpaRepository<Otp, Integer> {
	@Query(value = "SELECT * FROM otp WHERE SENT_TIME >= NOW() - INTERVAL 10 MINUTE AND EMAIL = ?1 AND OTP = ?2", nativeQuery = true)
	Otp findByEmailAndOtp(String emailAddress, String otp);

	boolean existsByEmail(String email);

	Otp findByEmail(String email);
}
