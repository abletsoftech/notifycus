package com.ablet.notifycus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.ablet.notifycus.model.CreditPlan;

public interface CreditPlanRepository extends JpaRepository<CreditPlan, Integer> {
	CreditPlan findByPlanName(String planName);

}
