package com.ablet.notifycus.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.ablet.notifycus.model.SchedulerHistory;

public interface SchedulerHistoryRepository extends JpaRepository<SchedulerHistory, Integer> {

	@Query(value = "SELECT * FROM  schedulerhistory WHERE DATE(NEXT1DAYS_SCHEDULER) = CURDATE()", nativeQuery = true)
	SchedulerHistory getechdulerHistoryOnToday();
	
}
