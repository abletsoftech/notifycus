package com.ablet.notifycus.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.User;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	Customer findByCustomerID(int cusID);

	@Query(value = "SELECT * FROM customer WHERE DATE(CREATED_DATE) = CURDATE() AND USER_ID = ?1 AND DELETED=false", nativeQuery = true)
	List<Customer> customerDetailsByToday(int userID);

	@Query(value = "SELECT * FROM customer WHERE EXP_DATE BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL 2 DAY) AND DELETED=false", nativeQuery = true)
	List<Customer> pucExpiryNext2Days();

	@Query(value = "SELECT * FROM customer WHERE USER_ID= ?1 AND DELETED=false", nativeQuery = true)
	List<Customer> customerDetailsForUser(int userID);

	@Query(value = "SELECT * FROM customer WHERE EXP_DATE BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL 1 DAY) AND DELETED=false AND NOTIFY2=false", nativeQuery = true)
	List<Customer> pucExpiryWithinNext1Days();

	@Query(value = "SELECT * FROM customer WHERE EXP_DATE=CURDATE() AND DELETED=false AND NOTIFY1=false", nativeQuery = true)
	List<Customer> pucExpireToday();

	@Query(value = "SELECT * FROM customer WHERE EXP_DATE=CURDATE() AND USER_ID=?1 AND DELETED=false", nativeQuery = true)
	List<Customer> pucExpireTodayByUserID(int userID);


}
