package com.ablet.notifycus.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ablet.notifycus.model.CreateOrder;
import com.ablet.notifycus.model.Customer;

public interface CreateOrderRepository extends JpaRepository<CreateOrder,String> {
	@Query(value = "SELECT * FROM createorder WHERE CREATE_ORDER_ID = ?1 AND USER_ID = ?2", nativeQuery = true)
	CreateOrder findBycreateOrderIDAndUserID(String createOrderID, int userID);
	CreateOrder findBycreateOrderID(String createOrderID);

}
