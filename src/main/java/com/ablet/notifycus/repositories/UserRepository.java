package com.ablet.notifycus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ablet.notifycus.model.User;


public interface UserRepository extends JpaRepository<User, Integer> {
	boolean existsByUserID(int userID);
	User findByEmail(String email);
	User findByUserID(int userID);
	boolean existsByEmail(String email);
	@Query(value = "SELECT PASSWORD FROM USER WHERE EMAIL= ?1 AND ROLE='USER'", nativeQuery = true)
	String userLogin(String email);
	@Query(value = "SELECT PASSWORD FROM USER WHERE EMAIL= ?1 AND ROLE='ADMIN'", nativeQuery = true)
	String adminLogin(String email);

}
