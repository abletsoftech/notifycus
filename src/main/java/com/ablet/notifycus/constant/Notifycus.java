package com.ablet.notifycus.constant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.ablet.notifycus.utils.Scheduler;

@SpringBootApplication
@ComponentScan(basePackages={"com.ablet.notifycus.*"})
@EntityScan(basePackages={"com.ablet.notifycus.model"})
@EnableJpaRepositories(basePackages = "com.ablet.notifycus.repositories")
@EnableScheduling
@EnableAsync
//@EnableWebMvc

public class Notifycus extends SpringBootServletInitializer{
	   
	public static void main(String[] args) {
		SpringApplication.run(Notifycus.class, args);
	}
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        // The Scheduler Class needs to be added manually, because it dont have to be defined as Bean
        return application.sources(Notifycus.class, Scheduler.class);
    }

}
