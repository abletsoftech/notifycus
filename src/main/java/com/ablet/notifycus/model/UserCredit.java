package com.ablet.notifycus.model;

import java.sql.Timestamp;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

import com.ablet.notifycus.utils.LocalDateDeserializer;
import com.ablet.notifycus.utils.LocalDateSerializer;
import com.ablet.notifycus.utils.TimestampDeserializer;
import com.ablet.notifycus.utils.TimestampSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
@Entity
@Component
@Table(name = "usercredit")
public class UserCredit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="USER_CREDIT_ID",nullable = false, insertable = false)
	@JsonIgnore
	private int userCreditID;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="USER_ID",nullable=false,insertable = true, referencedColumnName = "USER_ID")
	private User user;
	@Column(name="CREDIT",nullable = false, insertable = true)
	private int credit;
	@Column(name="REMAINING_CREDIT",nullable = false, insertable = true)
	private int remainingCredit;
	@Column(name="USED_CREDIT",nullable = false, insertable = true)
	private int usedCredit;
	@Column(name="PLAN_NAME",nullable = false, insertable = true)
	private String planName;
	@JsonIgnore
	@Column(name="ACTIVATED",nullable = false, insertable = false,columnDefinition="boolean default true")
	private boolean activated;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name="EXP_DATE",nullable = false,insertable = true)
	private LocalDate expiryDate;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	@Column(name="CREATED_DATE",nullable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp createdDate;
	public int getUserCreditID() {
		return userCreditID;
	}
	public void setUserCreditID(int userCreditID) {
		this.userCreditID = userCreditID;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}
	public int getRemainCredit() {
		return remainingCredit;
	}
	public void setRemainCredit(int remainingCredit) {
		this.remainingCredit = remainingCredit;
	}
	public int getUsedCredit() {
		return usedCredit;
	}
	public void setUsedCredit(int usedCredit) {
		this.usedCredit = usedCredit;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public boolean getActivated() {
		return activated;
	}
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	public LocalDate getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	} 
	

}
