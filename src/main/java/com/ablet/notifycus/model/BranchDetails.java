package com.ablet.notifycus.model;

import java.sql.Blob;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.ablet.notifycus.utils.TimestampDeserializer;
import com.ablet.notifycus.utils.TimestampSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@Entity
@Component
@Table(name = "branchDetails")
public class BranchDetails {
	@Id
	private int branchID;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@MapsId
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="USER_ID",unique=true,insertable = true, referencedColumnName = "USER_ID")
	private User user;
	@Column(name="BRANCH_NAME",nullable = false, insertable = true)
	private String branchName;
	@Column(name="BRANCH_PHOTO",nullable = false, insertable = true)
	private Blob branchPhoto;
	@Column(name="BRANCH_ADDRESS",nullable = false, insertable = true)
	private String branchAddress;
	@JsonIgnore
	@Column(name="SERVICES",nullable = false, insertable = false)
	private String services;
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	@Column(name="CREATED_DATE",nullable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp createdDate;
	public int getBranchID() {
		return branchID;
	}
	public void setBranchID(int branchID) {
		this.branchID = branchID;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Blob getBranchPhoto() {
		return branchPhoto;
	}
	public void setBranchPhoto(Blob branchPhoto) {
		this.branchPhoto = branchPhoto;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getServices() {
		return services;
	}
	public void setServices(String services) {
		this.services = services;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	} 

}
