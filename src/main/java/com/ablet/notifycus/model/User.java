package com.ablet.notifycus.model;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.springframework.stereotype.Component;
import com.ablet.notifycus.utils.TimestampDeserializer;
import com.ablet.notifycus.utils.TimestampSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Component
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="USER_ID",unique=true,nullable = false, insertable = false)
	private int userID;
	@Column(name="NAME",nullable = false, insertable = true)
	private String name;
	@Column(name="EMAIL",unique=true,nullable = false, insertable = true)
	private String email;
	@Column(name="COMPANY_NAME",nullable = false, insertable = true)
	private String companyName;
	@Column(name="MOBILE_NO",unique=true,nullable = false, insertable = true)
	private String mobileNo;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@Column(name="PASSWORD",nullable = false, insertable = true)
	private String password;
	@JsonIgnore
	@Column(name="ROLE",nullable = false,insertable = false,columnDefinition = "VARCHAR(255) DEFAULT 'user'")
	private String role;
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	@Column(name="CREATED_DATE",nullable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp createdDate; 
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval=true)
	private Collection<Customer> customer;
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval=true)
	private Collection<CreateOrder> createOrder;
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval=true)
	private Collection<UserCredit> userCredit;
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY,mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval=true)
	@PrimaryKeyJoinColumn
	private BranchDetails branchDetails;
	@Transient
	private String otp;
	@Transient
	private String message;
	
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public Collection<CreateOrder> getCreateOrder() {
		return createOrder;
	}
	public void setCreateOrder(Collection<CreateOrder> createOrder) {
		this.createOrder = createOrder;
	}
	
	public Collection<Customer> getCustomer() {
		return customer;
	}
	public void setCustomer(Collection<Customer> customer) {
		this.customer = customer;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
}
