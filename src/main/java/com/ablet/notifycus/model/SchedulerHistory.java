package com.ablet.notifycus.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.stereotype.Component;

import com.ablet.notifycus.utils.TimestampDeserializer;
import com.ablet.notifycus.utils.TimestampSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Component
@Table(name="schedulerhistory")
public class SchedulerHistory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SCHEDULER_ID",nullable = false,insertable=false)
	private int schedulerHistoryID;
	@Column(name = "NEXT1DAYS_SCHEDULER",nullable=true,insertable=false)
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp next1DayScheduler;
	@Column(name = "TODAY_SCHEDULER",nullable=true,insertable=false)
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp todayScheduler;
	
	public int getSchedulerHistoryID() {
		return schedulerHistoryID;
	}
	public void setSchedulerHistoryID(int schedulerHistoryID) {
		this.schedulerHistoryID = schedulerHistoryID;
	}
	public Timestamp getNext1DayScheduler() {
		return next1DayScheduler;
	}
	public void setNext1DayScheduler(Timestamp next1DayScheduler) {
		this.next1DayScheduler = next1DayScheduler;
	}
	public Timestamp getTodayScheduler() {
		return todayScheduler;
	}
	public void setTodayScheduler(Timestamp todayScheduler) {
		this.todayScheduler = todayScheduler;
	}

	

	
	
}
