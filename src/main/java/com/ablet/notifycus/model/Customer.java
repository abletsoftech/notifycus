package com.ablet.notifycus.model;

import java.sql.Timestamp;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

import com.ablet.notifycus.utils.LocalDateDeserializer;
import com.ablet.notifycus.utils.LocalDateSerializer;
import com.ablet.notifycus.utils.TimestampDeserializer;
import com.ablet.notifycus.utils.TimestampSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Component
@Table(name = "CUSTOMER")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CUSTOMER_ID",nullable = false, insertable = false)
	private int customerID;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="USER_ID",nullable=false,insertable = true, referencedColumnName = "USER_ID")//PrimaryKey
	private User user;
	@Column(name="MOBILE_NO",nullable = false,insertable = true)
	private String mobileNo;
	@Column(name="WORK_TYPE",nullable = false,insertable = true)
	private String workType;
	@Column(name="VEHICLE_NO",nullable = false,insertable = true)
	private String vehicleNo;
	@Column(name="VEHICLE_TYPE",nullable = false,insertable = true)
	private String vehicleType;
	@Column(name="EXP_DUR_MONTH",nullable = false,insertable = true)
	private int expiryDurationMonth;
	@Column(name="AMOUNT",nullable = false,insertable = true)
	private int amount;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@Column(name="EXP_DATE",nullable = false,insertable = true)
	private LocalDate expiryDate;
	@JsonIgnore
	@Column(name="DELETED",nullable = false,insertable = false,columnDefinition="tinyint(1) default 0")
	private boolean deleted;
	@Column(name="NOTIFY1",nullable = false,insertable = false,columnDefinition="tinyint(1) default 0")
	private boolean notify1;
	@Column(name="NOTIFY2",nullable = false,insertable = false,columnDefinition="tinyint(1) default 1")
	private boolean notify2;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	@Column(name="CREATED_DATE",nullable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private Timestamp createdDate;

	
	public boolean getNotify1() {
		return notify1;
	}

	public void setNotify1(boolean notify1) {
		this.notify1 = notify1;
	}

	public boolean getNotify2() {
		return notify2;
	}

	public void setNotify2(boolean notify2) {
		this.notify2 = notify2;
	}

	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	
	public String getWorkType() {
		return workType;
	}

	public void setWorkType(String workType) {
		this.workType = workType;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public int getExpiryDurationMonth() {
		return expiryDurationMonth;
	}

	public void setExpiryDurationMonth(int expiryDurationMonth) {
		this.expiryDurationMonth = expiryDurationMonth;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}


}
