package com.ablet.notifycus.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

import com.ablet.notifycus.utils.TimestampDeserializer;
import com.ablet.notifycus.utils.TimestampSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Component
@Table(name = "creditplan")
public class CreditPlan {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PLAN_ID", nullable = false, insertable = false)
	private int planID;
	@Column(name = "PLAN_NAME", nullable = false, insertable = true)
	private String planName;
	@Column(name = "TOTAL_CREDIT", nullable = false, insertable = true)
	private int totalCredit;
	@Column(name = "AMOUNT", nullable = false, insertable = true)
	private int amount;
	@Column(name = "VALIDITY_MONTH", nullable = false, insertable = true)
	private int validityInMonth;
	@Column(name = "ACTIVATED", nullable = false,insertable = false, columnDefinition="boolean default true")
	private boolean activated;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	@Column(name = "CREATED_DATE", nullable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp createdDate;
	public int getPlanID() {
		return planID;
	}
	public void setPlanID(int planID) {
		this.planID = planID;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public int getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(int totalCredit) {
		this.totalCredit = totalCredit;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getValidityInMonth() {
		return validityInMonth;
	}
	public void setValidityInMonth(int validityInMonth) {
		this.validityInMonth = validityInMonth;
	}
	public boolean getActivated() {
		return activated;
	}
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	
}
