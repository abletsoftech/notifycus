package com.ablet.notifycus.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.stereotype.Component;

import com.ablet.notifycus.utils.TimestampDeserializer;
import com.ablet.notifycus.utils.TimestampSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Component
@Table(name = "createorder")
public class CreateOrder {
	@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CREATE_ORDER_ID", nullable = false, insertable = true)
	private String createOrderID;
	
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="USER_ID",nullable=false,referencedColumnName = "USER_ID")//PrimaryKey
	private User user;
	@Column(name = "PAYMENT_ID",nullable = true)
	private String paymentID;
	@Column(name = "SIGNATURE",nullable = true)
	private String signature;
	@Column(name = "PLAN_NAME",nullable = true)
	private String planName;
	@Column(name = "TOTAL_CREDIT",nullable = true)
	private int totalCredit;
	@Column(name = "TOTAL_AMOUNT",nullable = true)
	private int totalAmount;
	@JsonSerialize(using =TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	@Column(name = "CREATED_DATE",nullable = false, insertable = false,columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp createdDate;
	@Transient
	private String message;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getCreateOrderID() {
		return createOrderID;
	}

	public void setCreateOrderID(String createOrderID) {
		this.createOrderID = createOrderID;
	}


	public String getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public int getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(int totalCredit) {
		this.totalCredit = totalCredit;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
