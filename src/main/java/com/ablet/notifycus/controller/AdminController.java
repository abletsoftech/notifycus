package com.ablet.notifycus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.SchedulerHistory;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.repositories.CreditPlanRepository;
import com.ablet.notifycus.repositories.OtpRepository;
import com.ablet.notifycus.repositories.UserRepository;
import com.ablet.notifycus.validation.UserValidation;


@Controller
@CrossOrigin
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private UserValidation userValidation;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private OtpRepository otpRepository;
	@Autowired
	private CreditPlanRepository creditPlanRepository;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public User adminLogin(@RequestBody User user) {
		//return userValidation.userLogin(user);
		return user;

	}


}
