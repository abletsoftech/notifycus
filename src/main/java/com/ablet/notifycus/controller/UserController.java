package com.ablet.notifycus.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.ablet.notifycus.model.CreateOrder;
import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.model.UserCredit;
import com.ablet.notifycus.repositories.CreditPlanRepository;
import com.ablet.notifycus.repositories.CustomerRepository;
import com.ablet.notifycus.repositories.OtpRepository;
import com.ablet.notifycus.repositories.SchedulerHistoryRepository;
import com.ablet.notifycus.repositories.UserCreditRepository;
import com.ablet.notifycus.repositories.UserRepository;
import com.ablet.notifycus.validation.UserValidation;

@Controller
@CrossOrigin
@RequestMapping("/user")

public class UserController {

	@Autowired
	private UserValidation userValidation;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CreditPlanRepository creditPlanRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private SchedulerHistoryRepository schedulerHistoryRepository;
	@Autowired
	private UserCreditRepository userCreditRepository;

	@PostMapping("/registrationOtp")
	@ResponseBody
	public User registrationOtp(@RequestBody User user) {
		return userValidation.registrationOtp(user);
	}

	@PostMapping("/userRegistration")
	@ResponseBody
	public String userRegistration(@RequestBody User user) {
		return userValidation.userRegistration(user);

	}

	@GetMapping("/login")
	@ResponseBody
	public String userLogin(HttpServletRequest request) {
		return userValidation.userLogin(request.getHeader("email"), request.getHeader("password"));
	}

	@GetMapping("/resetPasswordOtp")
	@ResponseBody
	public String resetPasswordOtp(@RequestParam String email) {
		return userValidation.resetPasswordOtp(email);

	}

	@PostMapping("/resetPassword")
	@ResponseBody
	public String resetPassword(@RequestBody User user) {
		return userValidation.resetPassword(user);

	}

	@PostMapping("/addCustomerDetails")
	@ResponseBody
	public String addCustomerDetails(@RequestBody Customer customer, HttpServletRequest request) {
		return userValidation.addCustomerDetails(customer, request);
	}

	@GetMapping("/customerDetailsForUser")
	@ResponseBody
	public List<Customer> customerDetailsForUser(HttpServletRequest request) {
		return userValidation.customerDetailsForUser(request);

	}

	@PostMapping("/updateCustomerDetails")
	@ResponseBody
	public String updateCustomerDetails(@RequestBody Customer customer, HttpServletRequest request) {
		return userValidation.updateCustomerDetails(customer, request);

	}

	@PostMapping("/deleteCustomerDetails")
	@ResponseBody
	public List<Customer> deleteCustomerDetails(HttpServletRequest request) {
		return null;

	}

	@GetMapping(value = "/totalTodaysCustForUser")
	@ResponseBody
	public int totalTodaysCustForUser(HttpServletRequest request) {
		return userValidation.totalTodaysCustForUser(request);
	}

	@GetMapping(value = "/numOfTotalCusForUser")
	@ResponseBody
	public int numOfTotalCusForUser(HttpServletRequest request) {
		return userValidation.numOfTotalCusForUser(request);
	}

	@GetMapping(value = "/pucExpTodayForUser")
	@ResponseBody
	public List<Customer> pucExpTodayForUser(HttpServletRequest request) {
		return userValidation.pucExpTodayForUser(request);
	}

	@GetMapping(value = "/buyCredit")
	@ResponseBody
	public CreateOrder buyCredit(@RequestParam String planName, HttpServletRequest request) {
		return userValidation.buyCredit(planName, request);
	}

	@RequestMapping(value = "/paymentVerification", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String paymentVerification(@RequestBody CreateOrder createOrder, HttpServletRequest request) {
		return userValidation.paymentVerification(createOrder, request);
	}

	@GetMapping(value = "/test")
	@ResponseBody
	public User test(HttpServletRequest request, @RequestParam int id) throws Exception {
		System.out.println(request.getHeader("name"));
		return userRepository.findByUserID(id);

	}

	@GetMapping(value = "/test1")
	@ResponseBody
	public User test1(HttpServletRequest request, @RequestParam int id) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		// Send request with GET method and default Headers.
		String url = "http://localhost:8080/notifycus/user/test?id=" + id;
		// ResponseJson responseJson = restTemplate.getForObject(url,
		// ResponseJson.class);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("name", "prp");
		// request entity is created with request headers
		HttpEntity<User> req = new HttpEntity<User>(headers);
		ResponseEntity<User> responseEntity = restTemplate.exchange(url, HttpMethod.GET, req, User.class);
		return responseEntity.getBody();

	}

	@GetMapping(value = "/test2")
	@ResponseBody
	public User test2(HttpServletRequest request, @RequestParam int id) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		// Send request with GET method and default Headers.
		String url = "http://localhost:8080/notifycus/user/test?id=" + id;
		// ResponseJson responseJson = restTemplate.getForObject(url,
		// ResponseJson.class);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("name", "prp");
		// request entity is created with request headers
		HttpEntity<User> req = new HttpEntity<User>(headers);
		return restTemplate.exchange(url, HttpMethod.GET, req, User.class).getBody();

	}
}
