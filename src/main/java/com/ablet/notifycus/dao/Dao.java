package com.ablet.notifycus.dao;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.ablet.notifycus.mail.SendMail;
import com.ablet.notifycus.model.CreateOrder;
import com.ablet.notifycus.model.CreditPlan;
import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.Otp;
import com.ablet.notifycus.model.SchedulerHistory;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.model.UserCredit;
import com.ablet.notifycus.repositories.CreateOrderRepository;
import com.ablet.notifycus.repositories.CreditPlanRepository;
import com.ablet.notifycus.repositories.CustomerRepository;
import com.ablet.notifycus.repositories.OtpRepository;
import com.ablet.notifycus.repositories.SchedulerHistoryRepository;
import com.ablet.notifycus.repositories.UserCreditRepository;
import com.ablet.notifycus.repositories.UserRepository;
import com.ablet.notifycus.utils.CreateNewOrder;
import com.ablet.notifycus.utils.FileBodyOperation;
import com.ablet.notifycus.utils.OtpGenerator;

@Component
public class Dao {

	Logger logs = LoggerFactory.getLogger(Dao.class);
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private OtpRepository otpRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private FileBodyOperation fileBodyOperation;
	@Autowired
	private SendMail sendMail;
	@Autowired
	private CreateNewOrder createNewOrder;
	@Autowired
	private CreateOrderRepository createOrderRepository;
	@Autowired
	private CreditPlanRepository creditPlanRepository;
	@Autowired
	private SchedulerHistoryRepository schedulerHistoryRepository;
	@Autowired
	private Otp otp;
	@Autowired
	private UserCreditRepository userCreditRepository;
	@Autowired
	private UserCredit userCredit;

	@Transactional
	public void registrationOtp(User user) throws Exception {
		if (otpRepository.existsByEmail(user.getEmail()))
			otp = otpRepository.findByEmail(user.getEmail());
		else
		otp.setEmail(user.getEmail());
		otp.setOtp(OtpGenerator.generateOtp());
		sendMail.sentOtp(user.getEmail(), fileBodyOperation.setOtpEmail(user.getEmail(), otp.getOtp(),true),true);
		otpRepository.saveAndFlush(otp);
	}

	@Transactional
	public void userRegistration(User user) throws Exception {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.saveAndFlush(user);
	}

	public void resetPasswordOtp(String email) throws Exception {
		Otp otp = new Otp();
		if (otpRepository.existsByEmail(email))
			otp = otpRepository.findByEmail(email);
		else
			otp.setEmail(email);
		otp.setOtp(OtpGenerator.generateOtp());
		sendMail.sentOtp(email, fileBodyOperation.setOtpEmail(email, otp.getOtp(),false),false);
		otpRepository.saveAndFlush(otp);

	}

	public void resetPassword(User user) {
		User user1 = userRepository.findByEmail(user.getEmail());
		userRepository.saveAndFlush(user1);
	}

	public UserCredit userCreditOperataion(Customer customer) {

		if (userCreditRepository.currentActiveUserPlan(customer.getUser().getUserID()) != null) {
			userCredit = userCreditRepository.currentActiveUserPlan(customer.getUser().getUserID());
			userCredit.setRemainCredit(userCredit.getRemainCredit() - 1);
			userCredit.setUsedCredit(userCredit.getUsedCredit() + 1);

		} else if (userCreditRepository.allActiveUserPlan(customer.getUser().getUserID()) != null) {
			List<UserCredit> uc = userCreditRepository.allActiveUserPlan(customer.getUser().getUserID());
			userCredit = uc.get(0);
			userCredit.setRemainCredit(userCredit.getRemainCredit() - 1);
			userCredit.setUsedCredit(userCredit.getUsedCredit() + 1);

		}
		return userCredit;
	}

	@Transactional
	public void addCustomerDetails(Customer customer) throws Exception {
		customer.setExpiryDate(customer.getExpiryDate().plusMonths(customer.getExpiryDurationMonth()));
		if (customer.getWorkType().equalsIgnoreCase("pollution"))
			customer.setWorkType(customer.getWorkType() + "(PUC)");
		customerRepository.saveAndFlush(customer);
		userCreditRepository.saveAndFlush(userCreditOperataion(customer));

	}

	@Transactional
	public void updateCustomerDetails(Customer customer) throws Exception {
		customer.setExpiryDate(customer.getExpiryDate().plusMonths(customer.getExpiryDurationMonth()));
		if (customer.getWorkType().equalsIgnoreCase("pollution"))
			customer.setWorkType(customer.getWorkType() + "(PUCC)");
		customerRepository.saveAndFlush(customer);

	}

	@Transactional
	public void deleteCustomerDetails(Customer customer) throws Exception {
		Customer cust = customerRepository.findByCustomerID(customer.getCustomerID());
		cust.setDeleted(true);
		customerRepository.saveAndFlush(cust);

	}

	public List<Customer> pucExpiryNext2Days() {
		return customerRepository.pucExpiryNext2Days();
	}

	public List<Customer> customerDetailsForUser(int userID) {
		return customerRepository.customerDetailsForUser(userID);
	}

	public List<User> getAllUserDetails() {
		return (List<User>) userRepository.findAll();
	}

	public UserCredit addCreditForUser(CreateOrder createOrder) throws Exception {
		CreditPlan plan = creditPlanRepository.findByPlanName(createOrder.getPlanName());
		userCredit.setCredit(plan.getTotalCredit());
		userCredit.setExpiryDate(LocalDate.now(ZoneId.of("Asia/Kolkata")).plusMonths(plan.getValidityInMonth()));
		userCredit.setPlanName(plan.getPlanName());
		userCredit.setRemainCredit(plan.getTotalCredit());
		userCredit.setUser(createOrder.getUser());
		return userCredit;
	}

	@Transactional
	public CreateOrder buyCredit(CreateOrder createOrder) throws Exception {
		createOrder = createNewOrder.createOrder(createOrder);
		return createOrderRepository.saveAndFlush(createOrder);
	}

	@Transactional
	public boolean paymentVerification(CreateOrder createOrder) throws Exception {
		// check signature
		// if true then update creditPlan table and capture payments
		// else false then return failure payment
		if (createNewOrder.paymentVerification(createOrder)) {
			CreateOrder cr = createOrderRepository.findBycreateOrderID(createOrder.getCreateOrderID());
			cr.setPaymentID(createOrder.getPaymentID());
			cr.setSignature(createOrder.getSignature());
			userCreditRepository.saveAndFlush(addCreditForUser(createOrder));
			createOrderRepository.saveAndFlush(cr);
			return true;
		} else {
			return false;
		}
	}

	public List<SchedulerHistory> getSechdulerHistoryFromDatabase() throws Exception {
		return (List<SchedulerHistory>) schedulerHistoryRepository.findAll();
	}

}
