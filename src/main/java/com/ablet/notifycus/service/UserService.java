package com.ablet.notifycus.service;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import com.ablet.notifycus.dao.Dao;
import com.ablet.notifycus.model.CreateOrder;
import com.ablet.notifycus.model.Customer;
import com.ablet.notifycus.model.Otp;
import com.ablet.notifycus.model.SchedulerHistory;
import com.ablet.notifycus.model.User;
import com.ablet.notifycus.repositories.CreateOrderRepository;
import com.ablet.notifycus.repositories.UserRepository;

@Service
public class UserService {
	Logger logs = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private Dao dao;
	@Autowired
	private CreateOrderRepository createOrderRepository;

	public String registrationOtp(User user) throws Exception {
		dao.registrationOtp(user);
		return "Email verification mail sent to your email,Please check your email";
	}

	public String userRegistration(User user) throws Exception {
		dao.userRegistration(user);
		return "Registration Successfull";
	}

	public String resetPasswordOtp(String email) throws Exception {
		dao.resetPasswordOtp(email);
		return "Password reset link sent to your email, Please check your email.";
	}

	public String userLogin(String email, String password) throws Exception {

		return "ok";
	}

	public String adminLogin(String email, String password) throws Exception {
		return "ok";

	}

	public String resetPassword(User user) throws Exception {
		dao.resetPassword(user);
		return "password reset successfully";
	}

	public String addCustomerDetails(Customer customer) throws Exception {
		dao.addCustomerDetails(customer);
		return "Customer Details added Successfully";
	}

	public String updateCustomerDetails(Customer customer) throws Exception {
		dao.updateCustomerDetails(customer);
		return "Customer Details updated Successfully";
	}

	public String deleteCustomerDetails(Customer customer) throws Exception {
		dao.deleteCustomerDetails(customer);
		return "Customer Details updated Successfully";
	}

	public List<Customer> planExpiryWithin3Days() throws Exception {
		return dao.pucExpiryNext2Days();
	}

	public List<Customer> customerDetailsForUser(int userID) throws Exception {
		return dao.customerDetailsForUser(userID);
	}

	

	public List<User> getAllUserDetails() throws Exception {
		return dao.getAllUserDetails();
	}

	public CreateOrder buyCredit(CreateOrder createOrder) throws Exception {
		return dao.buyCredit(createOrder);
	}

	public String paymentVerification(CreateOrder createOrder) throws Exception {
		if (dao.paymentVerification(createOrder))
			return "Credit Added Sucessfully";
		return "Payments Verification Failure";

	}

	public List<SchedulerHistory> getSechdulerHistoryFromDatabase() throws Exception {
		return dao.getSechdulerHistoryFromDatabase();
	}

}
