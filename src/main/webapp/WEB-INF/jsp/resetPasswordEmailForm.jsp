<!DOCTYPE html>
<html>
<title>Reset Password</title>
<link
	href="data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAMMOAADDDgAAAAAAAAAAAAD///////////////////////////////////////////z8/P///////////////////////////+7u7v/BwcH///////v7+//8/Pz//v7+//////////////////7+/v//////19fX/4yMjP9ubm7/iIiI/3Fxcf8TExP/VlZW///////////////////////7+/v//Pz8//39/f//////ioqK/wICAv8AAAD/AAAA/wAAAP8AAAD/AAAA/7S0tP9nZ2f/ZmZm/7a2tv/5+fn/////////////////s7Oz/wAAAP8JCQn/YWFh/xcXF/9bW1v/IiIi/wAAAP9paWn/mJiY/wAAAP8AAAD/FRUV/2JiYv9XV1f//////1paWv8AAAD/CQkJ/9XV1f9wcHD/srKy/5OTk/8AAAD/ERER//Dw8P8VFRX/jY2N/4aGhv8YGBj/W1tb//////9VVVX/AAAA/xsbG//y8vL/gICA/9fX1/+VlZX/AAAA/xAQEP//////FRUV/4aGhv///////v7+////////////pKSk/wAAAP8LCwv/HBwc/xMTE/8ZGRn/FxcX/wAAAP9cXFz/jY2N/wAAAP/MzMz///////////////////////////+EhIT/AAAA/wAAAP8AAAD/AAAA/wAAAP9MTEz/9fX1/xUVFf9nZ2f///////b29v+urq7/wcHB/+bm5v+ioqL/5OTk/8/Pz/91dXX/WFhY/3BwcP+zs7P///////////8AAAD/qKio///////r6+v/AAAA/ygoKP+tra3/AAAA/25ubv/////////////////19fX//Pz8///////9/f3/AAAA/5WVlf//////9fX1/xYWFv+JiYn/wsLC/wQEBP/FxcX///////7+/v/k5OT/BAQE/7Ozs////////Pz8/0FBQf8sLCz///////////+Xl5f/jY2N//////9ra2v/xcXF////////////gYGB/wMDA//x8fH///////7+/v/V1dX/AAAA/2FhYf////////////n5+f//////////////////////rq6u/wAAAP+MjIz///////v7+////////////7i4uP8AAAD/IyMj/5+fn//i4uL/8PDw/+fn5//AwMD/UFBQ/wAAAP9vb2////////7+/v////////////z8/P//////39/f/1VVVf8ICAj/BQUF/w8PD/8KCgr/AgIC/zAwMP+0tLT////////////+/v7/////////////////+/v7////////////7Ozs/7q6uv+ioqL/rq6u/9ra2v////////////z8/P/+/v7/////////////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="
	rel="icon" type="image/x-icon" />

<head>
<style>
*, *:before, *:after {
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}

body {
	font-family: 'Lato', sans-serif;
	background: linear-gradient(to top, #000000, #000000) fixed;
}

/***** For Smartphones *******/
.container-center {
	position: absolute;
	left: 50%;
	width: 85%;
	height: auto;
	background-color: transparent;
	-webkit-transition: all 0.1s;
	transition: all 0.1s;
	bottom: 50%;
	-webkit-transform: translateX(-50%) translateY(50%);
	transform: translateX(-50%) translateY(50%);
}

h2, img {
	text-align: center;
	color: white;
	font-weight: 10;
	text-shadow: 0px 1px rgba(0, 0, 0, 0.3);
}

h4 {
	text-align: center;
	color: black;
	font-size: 1.1em;
	font-family: times;
	font-style: normal;
	line-height: 130%;
	opacity: .6;
}

form {
	width: 100%;
	overflow: hidden;
	background-color: #FEFEFE;
	padding: 21px 13px;
	border-radius: 21px;
	-webkit-box-shadow: 0px 5px 34px rgba(0, 0, 0, 0.1);
	box-shadow: 0px 5px 34px rgba(0, 0, 0, 0.1);
}

formgroup {
	position: relative;
	width: 100%;
	display: block;
	margin: 1em 0;
	font-size: 1em;
}

formgroup input {
	width: 100%;
	border: none;
	border-bottom: 1px solid #888888;
	padding: 8px 0;
	font-size: inherit !important;
	margin-bottom: 13px;
	outline: none;
	opacity: 0.7;
	font-weight: 600;
}
formgroup input:focus {
	opacity: 1;
  border-bottom: 2px solid #000000;
  color: #888888;
}

formgroup label {
	position: absolute;
	font-size: 0.8em;
	top: -1em;
	left: 0;
	-webkit-transition: all 0.3s;
	transition: all 0.3s;
	opacity: 0.7;
	color: #888888;
	text-transform: uppercase;
}

formgroup span {
	position: absolute;
	top: -1em;
	left: -500px;
	opacity: 0;
	color: #333333;
	font-weight: bold;
	text-transform: uppercase;
	font-size: 0.8em;
	-webkit-transition: all 0.3s;
	transition: all 0.3s;
}

formgroup input:focus+label {
	left: 500px;
	opacity: 0;
}

formgroup input:focus ~ span {
	left: 0;
	opacity: 1;
}

.forgot {
	display: block;
	width: 100%;
	text-align: center;
	font-size: 1em;
	font-weight: bold;
	margin-top: 21px;
	opacity: 0.8;
}

#login-btn {
	border: none;
	color: white;
	padding: 0.8em 0;
	font-size: 1em;
	font-weight: 300;
	width: 100%;
	border-radius: 55px;
	-webkit-box-shadow: #5E7C9C;
	box-shadow: #5E7C9C;
	background: -webkit-gradient(linear, left top, right top, from(#5E7C9C),
		to(#5E7C9C));
	background: linear-gradient(to right, #000000, #000000);
	background-size: 100%;
	text-shadow: 0px 1px 2px rgba(0, 0, 0, 0.2);
}

p {
	color: white;
	text-align: center;
}

p a {
	color: inherit;
	text-decoration: none;
	font-weight: bold;
}

/***** For Tablets *******/
@media screen and (min-width: 330px) {
	.container-center {
		width: 70%;
	}
	#login-btn {
		padding: 0.8em 0;
		font-size: 1.2em;
	}
}
/***** For Desktop Monitors *******/
@media screen and (min-width: 768px) {
	.container-center {
		width: 500px;
	}
}
{
}
</style>

</head>
<html lang="en">
<head>
</head>
<body>
	<div class="container-center">
		<h2>Don't Worry!</h2>
		<form action="/notifycus/user/sentResetPasswordLink" method="post">
			<h4>Just provide your email.</h4>
			<h4>${message}</h4>
			<formgroup> <input type="text" name="email" id="email"
				value="" required
				pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
				title="Please enter valid email" /> 
				<label for="email">Email</label>
			<span>enter your email</span> </formgroup>
			<button id="login-btn">Reset password</button>
		</form>
		<p>
			Did you remember? <a href=https://www.google.com>Sign In</a>
		</p>
	</div>
</body>
</html>

