<!DOCTYPE html>
<html>
<head>
<style>
.button { 
border: #5C7C9C; 
background-color: #5C7C9C;
border-radius: 6px;
color: white;
padding: 7px 20px;
text-align: center;
text-decoration: none;
display: inline-block;
font-size: 16px;
margin: 4px 2px;
cursor: pointer;}
</style>
</head>
<body>
<div class="container">
<div class="box"><span></span>
<div class="content"><h1 style="color:#5C7C9C;">Welcome to the Notifycus family!</h1><br>
<p style="margin: 0;">Dear name,</p><br>Thanks for getting started with our Notifycus!<br>
We need a little more information to complete<br> your registration, including a confirmation of your email address.
<div>This OTP will be expire in 10 minute.</div>
</div><p>Your One Time Password (OTP) for Registration on Notifycus is <b>otp</b>.</p>
</div><p>Please note, this OTP is valid only for mentioned transaction and cannot be used for any other transaction.<br>
Please do not share this One Time Password with anyone. </p>
<div>Warm Regards,</div><div>Notifycus team</div><br><br><br><br>
<p style="font-size: 0.7em; text-align: center; padding: 4px;">
This message was send to you by Notifycus Teams</p>
<br><br><br></div></div></div></body></html>