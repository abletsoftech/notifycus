<!DOCTYPE html>
<html>
<title>Reset successful</title>
<link href="data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAMMOAADDDgAAAAAAAAAAAAD///////////////////////////////////////////z8/P///////////////////////////+7u7v/BwcH///////v7+//8/Pz//v7+//////////////////7+/v//////19fX/4yMjP9ubm7/iIiI/3Fxcf8TExP/VlZW///////////////////////7+/v//Pz8//39/f//////ioqK/wICAv8AAAD/AAAA/wAAAP8AAAD/AAAA/7S0tP9nZ2f/ZmZm/7a2tv/5+fn/////////////////s7Oz/wAAAP8JCQn/YWFh/xcXF/9bW1v/IiIi/wAAAP9paWn/mJiY/wAAAP8AAAD/FRUV/2JiYv9XV1f//////1paWv8AAAD/CQkJ/9XV1f9wcHD/srKy/5OTk/8AAAD/ERER//Dw8P8VFRX/jY2N/4aGhv8YGBj/W1tb//////9VVVX/AAAA/xsbG//y8vL/gICA/9fX1/+VlZX/AAAA/xAQEP//////FRUV/4aGhv///////v7+////////////pKSk/wAAAP8LCwv/HBwc/xMTE/8ZGRn/FxcX/wAAAP9cXFz/jY2N/wAAAP/MzMz///////////////////////////+EhIT/AAAA/wAAAP8AAAD/AAAA/wAAAP9MTEz/9fX1/xUVFf9nZ2f///////b29v+urq7/wcHB/+bm5v+ioqL/5OTk/8/Pz/91dXX/WFhY/3BwcP+zs7P///////////8AAAD/qKio///////r6+v/AAAA/ygoKP+tra3/AAAA/25ubv/////////////////19fX//Pz8///////9/f3/AAAA/5WVlf//////9fX1/xYWFv+JiYn/wsLC/wQEBP/FxcX///////7+/v/k5OT/BAQE/7Ozs////////Pz8/0FBQf8sLCz///////////+Xl5f/jY2N//////9ra2v/xcXF////////////gYGB/wMDA//x8fH///////7+/v/V1dX/AAAA/2FhYf////////////n5+f//////////////////////rq6u/wAAAP+MjIz///////v7+////////////7i4uP8AAAD/IyMj/5+fn//i4uL/8PDw/+fn5//AwMD/UFBQ/wAAAP9vb2////////7+/v////////////z8/P//////39/f/1VVVf8ICAj/BQUF/w8PD/8KCgr/AgIC/zAwMP+0tLT////////////+/v7/////////////////+/v7////////////7Ozs/7q6uv+ioqL/rq6u/9ra2v////////////z8/P/+/v7/////////////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==" rel="icon" type="image/x-icon" />

<head>
     <style>
    #card {
  position: relative;
  width: 320px;
  display: block;
  margin: 40px auto;
  text-align: center;
  font-family: 'Source Sans Pro', sans-serif;
}
#upper-side {
  padding: 2em;
  background-color: #000000;
  display: block;
  color: #fff;
  border-top-right-radius: 8px;
  border-top-left-radius: 8px;
}
#checkmark {
  font-weight: lighter;
  fill: #fff;
  margin: -3.5em auto auto 20px;
}
#status {
  font-weight: lighter;
  text-transform: uppercase;
  letter-spacing: 2px;
  font-size: 1em;
  margin-top: -.2em;
  margin-bottom: 0;
}
#lower-side {
  padding: 2em 2em 5em 2em;
  background: #fff;
  display: block;
  border-bottom-right-radius: 8px;
  border-bottom-left-radius: 8px;
}
#message {
  margin-top: -.5em;
  color: #757575;
  letter-spacing: 1px;
}
#contBtn {
  position: relative;
  top: 1.5em;
  text-decoration: none;
  background: #000000;
  color: #fff;
  margin: auto;
  padding: .8em 3em;
  border-radius: 25px;
  -webkit-transition: all .4s ease;
		-moz-transition: all .4s ease;
		-o-transition: all .4s ease;
		transition: all .4s ease;
}
#contBtn:hover {
  -webkit-box-shadow: 0px 15px 30px rgba(50, 50, 50, 0.41);
  -moz-box-shadow: 0px 15px 30px rgba(50, 50, 50, 0.41);
  box-shadow: 0px 15px 30px rgba(50, 50, 50, 0.41);
  -webkit-transition: all .4s ease;
		-moz-transition: all .4s ease;
		-o-transition: all .4s ease;
		transition: all .4s ease;
}
</style>
    
</head>
<body>
  <div id='card' class="animated fadeIn">
  <div id='upper-side'>
    <?xml version="1.0" encoding="utf-8"?>
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="checkmark" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">
        <path d="M131.583,92.152l-0.026-0.041c-0.713-1.118-2.197-1.447-3.316-0.734l-31.782,20.257l-4.74-12.65
	c-0.483-1.29-1.882-1.958-3.124-1.493l-0.045,0.017c-1.242,0.465-1.857,1.888-1.374,3.178l5.763,15.382
	c0.131,0.351,0.334,0.65,0.579,0.898c0.028,0.029,0.06,0.052,0.089,0.08c0.08,0.073,0.159,0.147,0.246,0.209
	c0.071,0.051,0.147,0.091,0.222,0.133c0.058,0.033,0.115,0.069,0.175,0.097c0.081,0.037,0.165,0.063,0.249,0.091
	c0.065,0.022,0.128,0.047,0.195,0.063c0.079,0.019,0.159,0.026,0.239,0.037c0.074,0.01,0.147,0.024,0.221,0.027
	c0.097,0.004,0.194-0.006,0.292-0.014c0.055-0.005,0.109-0.003,0.163-0.012c0.323-0.048,0.641-0.16,0.933-0.346l34.305-21.865
	C131.967,94.755,132.296,93.271,131.583,92.152z" />
        <circle fill="none" stroke="#ffffff" stroke-width="5" stroke-miterlimit="10" cx="109.486" cy="104.353" r="32.53" />
      </svg>
      <h3 id='status'>
      Success
    </h3>
  </div>
  <div id='lower-side'>
    <p id='message'>
      Congratulations, your password has been reset successfully.
    </p>
    <a href=https://www.google.com id="contBtn">Continue</a>
  </div>
</div>
</body>
</html>
