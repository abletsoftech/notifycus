<!DOCTYPE html>
<html>
<title>404 Error</title>
<link href="data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAMMOAADDDgAAAAAAAAAAAAD///////////////////////////////////////////z8/P///////////////////////////+7u7v/BwcH///////v7+//8/Pz//v7+//////////////////7+/v//////19fX/4yMjP9ubm7/iIiI/3Fxcf8TExP/VlZW///////////////////////7+/v//Pz8//39/f//////ioqK/wICAv8AAAD/AAAA/wAAAP8AAAD/AAAA/7S0tP9nZ2f/ZmZm/7a2tv/5+fn/////////////////s7Oz/wAAAP8JCQn/YWFh/xcXF/9bW1v/IiIi/wAAAP9paWn/mJiY/wAAAP8AAAD/FRUV/2JiYv9XV1f//////1paWv8AAAD/CQkJ/9XV1f9wcHD/srKy/5OTk/8AAAD/ERER//Dw8P8VFRX/jY2N/4aGhv8YGBj/W1tb//////9VVVX/AAAA/xsbG//y8vL/gICA/9fX1/+VlZX/AAAA/xAQEP//////FRUV/4aGhv///////v7+////////////pKSk/wAAAP8LCwv/HBwc/xMTE/8ZGRn/FxcX/wAAAP9cXFz/jY2N/wAAAP/MzMz///////////////////////////+EhIT/AAAA/wAAAP8AAAD/AAAA/wAAAP9MTEz/9fX1/xUVFf9nZ2f///////b29v+urq7/wcHB/+bm5v+ioqL/5OTk/8/Pz/91dXX/WFhY/3BwcP+zs7P///////////8AAAD/qKio///////r6+v/AAAA/ygoKP+tra3/AAAA/25ubv/////////////////19fX//Pz8///////9/f3/AAAA/5WVlf//////9fX1/xYWFv+JiYn/wsLC/wQEBP/FxcX///////7+/v/k5OT/BAQE/7Ozs////////Pz8/0FBQf8sLCz///////////+Xl5f/jY2N//////9ra2v/xcXF////////////gYGB/wMDA//x8fH///////7+/v/V1dX/AAAA/2FhYf////////////n5+f//////////////////////rq6u/wAAAP+MjIz///////v7+////////////7i4uP8AAAD/IyMj/5+fn//i4uL/8PDw/+fn5//AwMD/UFBQ/wAAAP9vb2////////7+/v////////////z8/P//////39/f/1VVVf8ICAj/BQUF/w8PD/8KCgr/AgIC/zAwMP+0tLT////////////+/v7/////////////////+/v7////////////7Ozs/7q6uv+ioqL/rq6u/9ra2v////////////z8/P/+/v7/////////////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==" rel="icon" type="image/x-icon" />

<head>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:400,100,300,500);

body { 
  background-color: #000000; 
  color: #fff;
  font-size: 70%;
  line-height: 1;
  font-family: "Roboto", sans-serif;
}

.button {
  font-weight: 300;
  color: #fff;
  font-size: 1.2em;
  text-decoration: none;
  border: 1px solid #efefef;
  padding: .5em;
  border-radius: 3px;
  float: left;
  margin: 6em 0 0 -155px;
  left: 53%;
  position: relative;
  transition: all .3s linear;
}

.button:hover {
  background-color: #007aff;
  color: #fff;
}

p {
  font-size: 3em;
  text-align: center;
  font-weight: 100;
}
div {
  font-size: 2em;
  text-align: center;
  font-weight: 100;
}

h1 {
  text-align: center;
  font-size: 15em;
  font-weight: 100;
  
}
</style>
    
</head>
<html lang="en">
<body>
 <h1>404</h1>
<p>Oops! Something is wrong.</p>
<div>We're sorry you might have experienced some issues,  please retry again.</div>
<a class="button" href=https://www.google.com><i class="icon-home"></i> Go back in initial page, is better.</a>
</body>
</html>

